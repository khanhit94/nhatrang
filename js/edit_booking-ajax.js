$(document).ready(function() {
	$("#accept").click(function() {//button accept clicked
		var id_last=$("#id_last").val();
		var name=$('.name').html();
		var gender=$('.gender').val();
		var address=$('.address').html();
		var city=$('.city').html();
		var country=$('.country').val();
		var email=$('.email').html();
		var phone=$('.phone').html();
		var room_type=$('.room_type').val();
		var child=$('.child').html();
		var people=$('.people').html();
		var date_from=$('.date_from').val();
		var date_go=$('.date_go').val();
		var note=$('.note').html();
		// var num_room_before=new Array();
		// var i=0;
		// $(".plane1").each(function(){
		// 	num_room_before[i] = this.value;
		// 	i++;
		// });
		var num_room=new Array();
		var i=0;
		$("input[name='num_room[]']").each(function () {
			var ischecked = $(this).is(":checked");
		  if (ischecked) {
        num_room[i] = this.value;//get id num_room checked
				i++;
      }
		});
		//console.log(num_room_before[1]);
		// var id_type=$("#plane").val();
		// var num_room=new Array();
		$.post("edit_booking.php",
			{ 
				id_last:id_last,
				name:name,
				gender:gender,
				address:address,
				city:city,
				country:country,
				email:email,
				phone:phone,
				room_type:room_type,
				//num_room_before:num_room_before,
				num_room:num_room,
				child:child,
				people:people,
				date_from:date_from,
				date_go:date_go,
				note:note
			},
		function(data){
			$("#edit").html(data);
		});			
	});
});