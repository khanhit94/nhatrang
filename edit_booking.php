<?php
include ("view/include/connect.php");
include ("validate.php");
$data=array();
$data1=array();
$id_last=$_POST['id_last'];
$data['name'] = isset($_POST['name']) ? mysqli_real_escape_string($conn,strip_tags($_POST['name'])):''; 
$data['gender']=$_POST['gender'];
$data['address'] = isset($_POST['address']) ? mysqli_real_escape_string($conn,strip_tags($_POST['address'])):''; 
$data['city'] = isset($_POST['city']) ? mysqli_real_escape_string($conn,strip_tags($_POST['city'])):''; 
$data['country']=$_POST['country'];
$data['email']=$_POST['email'];
$data['phone']=$_POST['phone'];
$data['room_type']=$_POST['room_type'];
//$data['num_room_before']=$_POST['num_room_before'];
$data['num_room']=(isset($_POST['num_room'])) ? $_POST['num_room']:'';
$data['child']=$_POST['child'];
$data['people']=$_POST['people'];
$data['date_from']=date("Y-m-d",strtotime($_POST['date_from']));
$data['date_go']=date("Y-m-d",strtotime($_POST['date_go']));;
$data1['note'] = isset($_POST['note']) ? sqlimy_real_escape_string($conn,strip_tags($_POST['note'])):'';
$is_num=array('child'=>$_POST['child'],
              'people'=>$_POST['people'],
              'phone'=>$_POST['phone']
              );

  // if(empty($data['num_room']))
  //   $data['num_room']=$data['num_room_before'];
$error=check_empty($data);
$error_isnum=check_isnum($is_num);
$error_phone=is_phone($data['phone']);
$error_email=is_email($data['email']);
$error_date='';
if($data['date_from'] > $data['date_go']) $error_date='date';
echo "<script type='text/javascript'>$('.table td').css('border','1px solid #ddd')</script>";//resert border table
if($error==null && $error_isnum==null && $error_phone==true && $error_email==true && $error_date==null) {
	if(empty($data['num_room'])) {
		$sql1= "UPDATE booking 
            SET child='{$data['child']}',people='{$data['people']}',date_from='{$data['date_from']}',date_go='{$data['date_go']}',note='{$data1['note']}'
		        WHERE id_customer=$id_last";
    check_conn($sql1);
  }//end if empty
	else {
	  $sql2="DELETE FROM booking WHERE id_customer=$id_last";
    if (check_conn($sql2)) {
     	foreach ($data['num_room'] as $num_room) {
   		  $sql3= "INSERT INTO booking (id_customer,id_room,child,people,date_from,date_go,note)
                VALUES ('{$id_last}','{$num_room}','{$data['child']}','{$data['people']}','{$data['date_from']}','{$data['date_go']}','{$data1['note']}') " ;
        if (check_conn($sql3)) {
          $last_id = mysqli_insert_id($conn);
        }
        $sql4= "UPDATE room1 SET status=1 WHERE id='{$num_room}'";
        check_conn($sql4);
      }//end foreach
    } 
  }
  $sql5= "UPDATE customer SET name='{$data['name']}',gender='{$data['gender']}',address='{$data['address']}',city='{$data['city']}',country='{$data['country']}',email='{$data['email']}',phone='{$data['phone']}'
		      WHERE id=$id_last";
  check_conn($sql5);
  echo "<script type='text/javascript'>
          $('.table td').css('border','1px solid #ddd');alert('Thành công!!!')
        </script>";
}//end if
else {
  if ($error!=null)
    check_empty_javascrip($error);
  else if ($error==null && $error_isnum!=null) {
    check_isnum_javascrip($error_isnum);
  }
  else if ($error_phone==false) {
    echo "<script type='text/javascript'>
            $('.phone').css('border','1px solid red');alert('Số điện thoại không đúng!')
          </script>";
  }
  else if($error_date=='date')
     echo "<script type='text/javascript'>
            $('.dd').css('border','1px solid red');alert('Ngày đến phải nhỏ hơn ngày đi!')
          </script>";
  else echo  "<script type='text/javascript'>
                $('.email').css('border','1px solid red');alert('Email không đúng định dạng!')
              </script>";
}
?>