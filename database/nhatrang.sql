-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2016 at 03:31 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nhatrang`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `child` int(20) NOT NULL,
  `people` int(20) NOT NULL,
  `date_from` date NOT NULL,
  `date_go` date NOT NULL,
  `note` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=234 ;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `id_customer`, `id_room`, `child`, `people`, `date_from`, `date_go`, `note`) VALUES
(233, 107, 5, 1, 1, '2016-11-03', '2016-11-05', ''),
(227, 106, 3, 1, 1, '2016-11-29', '2016-11-21', ''),
(225, 105, 2, 1, 1, '2016-11-08', '2016-11-14', ''),
(224, 105, 1, 1, 1, '2016-11-08', '2016-11-14', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=108 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `gender`, `address`, `city`, `country`, `email`, `phone`) VALUES
(107, 'khanh2', 1, 'điện bàn1', 'quảng nam', 'Việt Nam', 'lephuockhanh94@gmail.com', 1647673239),
(106, 'khanh1', 1, 'điện bàn', 'quảng nam', 'Việt Nam', 'lephuockhanh94@gmail.com', 1647673239),
(105, 'lê phước khánh', 1, 'điện bàn', 'quảng nam', 'Việt Nam', 'lepuockhanh94@gmal.com', 1647674239);

-- --------------------------------------------------------

--
-- Table structure for table `room1`
--

CREATE TABLE IF NOT EXISTS `room1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_room` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_type` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `room1`
--

INSERT INTO `room1` (`id`, `name_room`, `id_type`, `status`) VALUES
(1, '1T', 1, 1),
(2, '2T', 1, 1),
(3, '3T', 1, 1),
(4, '1V1', 2, 1),
(5, '2V1', 2, 1),
(6, '3V1', 2, 0),
(7, '1V2', 3, 0),
(8, '2V2', 3, 0),
(9, '3V2', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `name_type`) VALUES
(1, 'Thường'),
(2, 'VIP 1'),
(3, 'VIP 2');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
